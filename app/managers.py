from django.db import models, connection
from collections import namedtuple


class BaseManager(models.Manager):
    def dict_fetchall(self, cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    def dict_fetchone(self, cursor):
        columns = (x.name for x in cursor.description)
        result = dict(zip(columns, cursor.fetchone()))
        return result

    def namedtuple_fetchall(self, cursor):
        "Return all rows from a cursor as a namedtuple"
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]


class ProfileManager(BaseManager):
    def get_profile(self, pk):
        with connection.cursor() as cursor:
            cursor.execute("""SELECT 
                                profiles.user_id, 
                                app_user.first_name, 
                                app_user.last_name, 
                                groups."name" as "group", 
                                universitys."name" as "university"
                            FROM 
                                public.app_user app_user, 
                                public."groups" groups, 
                                public.profiles profiles, 
                                public.universitys universitys
                            WHERE 
                                groups.id = profiles.group_id
                                AND profiles.user_id = app_user.id
                                AND profiles.user_id = %s
                                AND universitys.id = groups.university_id""",
                           [pk])
            try:
                return self.dict_fetchone(cursor)
            except Exception:
                return {'errors': {'group': 'not set'}}

    def get_skills(self, pk):
        with connection.cursor() as cursor:
            cursor.execute("""SELECT profiles_skills."level", skills."name"
                              FROM 
                                  public.profiles profiles, 
                                  public.profiles_skills profiles_skills, 
                                  public.skills skills
                              WHERE 
                                  profiles.user_id = %s
                                  AND profiles_skills.profile_id = profiles.id
                                  AND skills.id = profiles_skills.id
                              LIMIT 1000""", [pk])
            return self.dict_fetchall(cursor)

    def get_courses(self, pk):
        with connection.cursor() as cursor:
            cursor.execute("""SELECT courses."header", courses_users.is_complete
                              FROM 
                                  public.courses courses, 
                                  public.courses_users courses_users, 
                                  public.profiles profiles
                              WHERE 
	                              profiles.user_id = %s
	                              AND courses_users.course_id = courses.id
	                              AND courses_users.profile_id = profiles.id
                              LIMIT 1000""", [pk])
            return self.dict_fetchall(cursor)

    def get_events(self, pk):
        response = []
        for event in self.get(id=pk).event_set.all()[:1000]:
            response.append({
                'id': event.id,
                'header': event.header,
                'description': event.description,
                'date_start': event.date_start,
                'date_end': event.date_end,
            })
        return response
