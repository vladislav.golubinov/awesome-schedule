from django.test import TestCase

from app.models import University, User, Group
# from django.contrib.auth import get_user_model
#
# User = get_user_model()


class UniversityModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        University.objects.create(
            name='Нижегородский Государственный Технический Университет')
        University.objects.create(
            name='Nizhny Novgorod State Technical University')

    def test_name_label(self):
        u = University.objects.get(id=1)
        field_label = u._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'name')

    def test_name_max_length(self):
        u = University.objects.get(id=1)
        max_length = u._meta.get_field('name').max_length
        self.assertEquals(max_length, 64)

    # def test_get_absolute_url(self):
    #     u = University.objects.get(id=1)
    #     self.assertEquals(u.get_absolute_url(), '/universitys/1')
