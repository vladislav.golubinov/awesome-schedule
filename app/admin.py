from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import Course, CoursesProfiles, Profile, Event, University,\
    Group, Skill, ProfilesSkills

User = get_user_model()

admin.site.register(Course)
admin.site.register(CoursesProfiles)
admin.site.register(Profile)
admin.site.register(User)
admin.site.register(Event)
admin.site.register(Group)
admin.site.register(University)
admin.site.register(Skill)
admin.site.register(ProfilesSkills)
