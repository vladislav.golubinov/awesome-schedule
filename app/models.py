from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from .managers import ProfileManager


class User(AbstractUser):
    pass


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class University(models.Model):
    name = models.CharField(max_length=64)

    class Meta:
        db_table = 'universitys'


class Group(models.Model):
    name = models.CharField(max_length=10)
    university = models.ForeignKey(University, on_delete=models.CASCADE)

    class Meta:
        db_table = 'groups'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True)
    objects = models.Manager()
    data = ProfileManager()

    class Meta:
        db_table = 'profiles'


class Skill(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField()

    class Meta:
        db_table = 'skills'


class ProfilesSkills(models.Model):
    LEVEL = (
        ('B', 'Beginner'),
        ('M', 'Middle'),
        ('S', 'Senior'),
        ('G', 'Geek'),
    )
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    level = models.CharField(max_length=2, choices=LEVEL, default='B')

    class Meta:
        db_table = 'profiles_skills'
        index_together = (('profile', 'skill'))
        unique_together = (('profile', 'skill'))


class Course(models.Model):
    header = models.CharField(max_length=128,
                              validators=[MinLengthValidator(1),
                                          MaxLengthValidator(128)])
    description = models.TextField()
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()

    class Meta:
        db_table = 'courses'


# Many to Many
class CoursesProfiles(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    is_complete = models.BooleanField(default=False)

    class Meta:
        db_table = 'courses_users'
        index_together = (('profile', 'course'))
        unique_together = (('profile', 'course'))


class Event(models.Model):
    profiles = models.ManyToManyField(Profile, blank=True)
    header = models.CharField(max_length=128,
                              validators=[MinLengthValidator(1),
                                          MaxLengthValidator(128)])
    description = models.TextField()
    date_start = models.DateTimeField()
    date_end = models.DateTimeField()

    class Meta:
        db_table = 'events'
