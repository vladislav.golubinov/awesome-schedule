# from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from .views import UserViewSet, CourseViewSet, EventViewSet

router = routers.DefaultRouter()
router.register('users', UserViewSet)
router.register('courses', CourseViewSet)
router.register('events', EventViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
