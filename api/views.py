from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.auth import get_user_model
from app.models import Profile, Course, Event, University, Skill
from .serializers import ProfileSerializer, CourseSerializer, \
    UserSerializer, EventSerializer

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()[:1000]
    serializer_class = (UserSerializer)
    permission_classes_by_action = {'create': [AllowAny],
                                    'default': [IsAuthenticated]}

    @action(detail=True, methods=['GET'])
    def profile(self, request, pk=None):
        response = Profile.data.get_profile(pk)
        # todo: Нужно подумать как использовать передачу через serializer
        return Response(response, status=status.HTTP_200_OK)

    @action(detail=True, methods=['GET'])
    def skills(self, request, pk=None):
        response = Profile.data.get_skills(pk)
        return Response(response, status=status.HTTP_200_OK)

    @action(detail=True, methods=['GET'])
    def courses(self, request, pk=None):
        response = Profile.data.get_courses(pk)
        return Response(response, status=status.HTTP_200_OK)

    @action(detail=True, methods=['GET'])
    def events(self, request, pk=None):
        response = Profile.data.get_events(pk)
        return Response(response, status=status.HTTP_200_OK)

    def get_permissions(self):
        try:
            # return permission_classes depending on `action`
            return [permission()
                    for permission in
                    self.permission_classes_by_action[self.action]]
        except KeyError:
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]


class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()[:1000]
    serializer_class = (CourseSerializer,)


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()[:1000]
    serializer_class = (EventSerializer,)
